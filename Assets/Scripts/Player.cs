﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : MonoBehaviour
{
    [SerializeField] private int _Level = 1;
    [SerializeField] private Animator _Animator;
    [SerializeField] private Rigidbody _RigidBody;
    [SerializeField] private float _MoveSpeed = 10.0f;
    [SerializeField] private float _RotationSpeed = 60.0f;
    [SerializeField] private float _JumpForce = 60.0f;
    [SerializeField] private float _Vertical = 0.0f;
    [SerializeField] private float _Horizontal = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        _Animator = GetComponent<Animator>();
        _RigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        InputMovement();
        Attack();
        Movement();
    }

    private void Attack()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _Animator.SetTrigger("Attack 01");
        }

        if (Input.GetMouseButtonDown(1))
        {
            _Animator.SetTrigger("Attack 02");
        }
    }

    private void Movement()
    {
        if (Input.GetKeyDown(KeyCode.Space) && _Vertical != 0)
        {
            _Animator.SetBool("Walk Forward", false);
            _Animator.SetTrigger("Jump");
            _RigidBody.AddForce(transform.up * _JumpForce);
        }
        else if (_Vertical != 0)
        {
            _Animator.SetBool("Walk Forward", true);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                _Vertical *= 2;
                _Animator.SetBool("Walk Forward", false);
                _Animator.SetBool("Run Forward", true);
            }
        }
        else
        {
            _Animator.SetBool("Run Forward", false);
            _Animator.SetBool("Walk Forward", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _Animator.SetTrigger("Jump");
        }

        if(Input.GetKeyDown(KeyCode.K))
        {
            _Animator.SetTrigger("Die");
        }
    }

    private void InputMovement()
    {
        _Vertical = Input.GetAxis("Vertical");
        _Horizontal = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate() 
    {
        _RigidBody.velocity = (transform.forward * _Vertical) * _MoveSpeed * Time.fixedDeltaTime;
        _RigidBody.AddForce(Physics.gravity, ForceMode.Acceleration);
        transform.Rotate((transform.up * _Horizontal) * _RotationSpeed * Time.fixedDeltaTime);
    }

    private void LevelUp()
    {
        _Level++;
        transform.localScale = new Vector3(2 + _Level, 2 + _Level, 2 + _Level);

        StartCoroutine(RequestLevelUp());
    }

    IEnumerator RequestLevelUp()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", "teddysot");
        form.AddField("level", _Level);

        UnityWebRequest www = UnityWebRequest.Post("http://localhost:5000/tamagotchi-codecamp/us-central1/api/levelup", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log($"Level up");
        }
    }

    public void SetLevel(int level)
    {
        _Level = level;
        transform.localScale = new Vector3(2 + _Level, 2 + _Level, 2 + _Level);
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "EXP")
        {
            LevelUp();
            Destroy(other.gameObject);
        }
    }
}
