﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;

public class Authentication : MonoBehaviour
{
    [SerializeField] private User _User = new User();
    [SerializeField] private TMP_InputField _UsernameInput;
    [SerializeField] private TMP_InputField _PasswordInput;
    [SerializeField] private GameObject[] _PlayerPrefabs;
    [SerializeField] private GameObject _UI;

    public void OnLoginClicked()
    {
        Debug.Log($"Username: {_UsernameInput.text}, Password: {_PasswordInput.text}");

        StartCoroutine(Login());
    }

    IEnumerator Login()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", _UsernameInput.text);
        form.AddField("password", _PasswordInput.text);

        UnityWebRequest www = UnityWebRequest.Post("http://localhost:5000/tamagotchi-codecamp/us-central1/api/login", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log($"Logged in successful");
            Debug.Log(www.downloadHandler.text);
            _User = JsonUtility.FromJson<User>(www.downloadHandler.text);

            int numChar = _User.character - 1;

            var monster = Instantiate(_PlayerPrefabs[numChar], new Vector3(8, -37, -180), Quaternion.identity);

            monster.GetComponent<Player>().SetLevel(_User.level);

            _UI.SetActive(false);
        }
    }
}

[Serializable]
public class User
{
    public string token;
    public int character;
    public int level;
}
