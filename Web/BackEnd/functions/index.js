require('dotenv')
const cors = require('cors')
const express = require('express')
const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
const app = express();
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

const { login, register } = require('./apis/auth')
const { checkout } = require('./apis/checkout')
const { balance } = require('./apis/balance')
const { levelup } = require('./apis/levelup')

app.post('/login', login);
app.post('/register', register);
app.post('/checkout', checkout);
app.get('/balance', balance)
app.post('/levelup', levelup)

exports.api = functions.https.onRequest(app); 