const { db } = require("../utils/admin");

exports.levelup = (req, res) => {
    const { username, level } = req.body

    db
        .collection('auth')
        .get()
        .then((users) => {
            users.forEach((user) => {
                if (user.data().username === username) {
                    db.collection('auth').doc(`${user.id}`).update({ level: Number(level) })
                }
            })

            return res.status(200).send()
        })
        .catch(err => {
            console.log(err);
            return res.status(500).send({ err })
        })
}