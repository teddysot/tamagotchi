const { db } = require("../utils/admin");

exports.balance = (req, res) => {
    const { username } = req.query
    db
        .collection('auth')
        .get()
        .then((users) => {
            users.forEach((user) => {
                if (user.data().username === username) {
                    return res.status(200).send({ balance: user.data().balance })
                }
            })
        })
        .catch(err => {
            console.log(err);
            return res.status(500).send({ err })
        })
}