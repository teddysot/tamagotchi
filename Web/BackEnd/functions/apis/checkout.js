const { db } = require("../utils/admin");

const omise = require('omise')({
    'publicKey': 'pkey_test_5m9tnuy531rixv6ieiy',
    'secretKey': 'skey_test_5m9tnzbaslit4yblcrh',
});

exports.checkout = async (req, res) => {
    const { amount, token, monsterIdx } = req.body
    const username = 'teddysot'

    try {
        const customer = await omise.customers.create({
            email: 'tamagotchi@tamagotchi.com',
            'description': 'Tamagotchi Tamagotchi',
            'card': token,
        })
        const chargeResult = await omise.charges.create({
            amount,
            'currency': 'thb',
            'customer': customer.id,
        })

        db
            .collection('auth')
            .get()
            .then((users) => {
                users.forEach((user) => {
                    if (user.data().username === username) {
                        db.collection('auth').doc(`${user.id}`).update({ balance: user.data().balance - (amount / 100), character: monsterIdx })
                    }
                })
            })
            .catch(err => {
                console.log(err);
                return res.status(500).send({ err })
            })

        res.status(200).send({ amount: chargeResult.amount, status: chargeResult.status })
    }
    catch (error) {
        console.log(error);
    }
}