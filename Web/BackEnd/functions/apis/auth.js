const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { db } = require("../utils/admin");


exports.login = (req, res) => {
    const { username, password } = req.body
    db
        .collection('auth')
        .get()
        .then((users) => {
            users.forEach((user) => {
                if (user.data().username === username) {
                    console.log({ username });
                    if (bcrypt.compareSync(password, user.data().password)) {
                        const token = jwt.sign({ id: user.id, username }, "Tamagotchi", { expiresIn: 3600 });
                        const character = user.data().character
                        const level = user.data().level
                        console.log({ character });
                        return res.send({ token, character, level })
                    } else {
                        return res.status(400).send({ token: null })
                    }
                }
            })
        })
        .catch(err => {
            console.log(err);
            return res.status(500).send({ err })
        })
}
exports.register = (req, res) => {
    const { username, password, character } = req.body;

    const salt = bcrypt.genSaltSync(Number(process.env.SALT_ROUND));
    const hashedPW = bcrypt.hashSync(password, salt);
6
    const newUser = { username, password: hashedPW, character, balance: 10000, level: 1 }
    db
        .collection('auth')
        .add(newUser)
        .then((doc) => {
            const resUser = newUser
            resUser.id = doc.id
            return res.send(resUser)
        })
}