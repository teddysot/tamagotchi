import React from 'react'
import Unity, { UnityContext } from "react-unity-webgl";
import { Tabs } from 'antd';
import { AppleOutlined, AndroidOutlined, BookOutlined, ShopOutlined } from '@ant-design/icons';
import Register from './containers/pages/Register/Register';
import Home from './containers/pages/Home/Home';
import Shop from './containers/pages/Shop/Shop';

const { TabPane } = Tabs;


function App() {
  const unityContext = new UnityContext({
    loaderUrl: "Build/Build.loader.js",
    dataUrl: "Build/Build.data",
    frameworkUrl: "Build/Build.framework.js",
    codeUrl: "Build/Build.wasm",
  });

  return (
    <div style={{ margin: "0", height: "100vh", overflow: 'hidden' }}>
      <Tabs tabBarStyle={{ marginBottom: 0 }} size="large" type="card" defaultActiveKey="1" animated centered>
        <TabPane style={{ display: 'flex', alignItems: 'center', width: '100vw', height: '100vh', flexDirection: 'column' }} tab={<div><AppleOutlined />Home</div>} key="1">
          <Home />
        </TabPane>
        <TabPane tab={<div><BookOutlined />Register</div>} key="2">
          <Register />
        </TabPane>
        <TabPane tab={<div><ShopOutlined />Shop</div>} key="3">
          <Shop />
        </TabPane>
        <TabPane tab={<div><AndroidOutlined />Play Game</div>} key="4">
          {/* <div style={{ width: "100vw", height: "95vh", backgroundColor: 'skyblue' }}>UNITY</div> */}
          <Unity height="100vh" width="100vw" unityContext={unityContext} />
        </TabPane>
      </Tabs>
    </div>
  );
}

export default App;
