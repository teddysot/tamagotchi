import React from 'react'

const Home = () => {
    return (
        <>
            <img style={{ margin: "50px 0", objectFit: 'contain' }} src="https://static.wikia.nocookie.net/tamagotchi/images/7/74/Tamagotchi%21_logo.gif/revision/latest?cb=20110922190716" alt="tamagotchi"></img>
            <div style={{ display: 'flex' }}>
                <img style={{ margin: "0 0" }} src="https://i.imgur.com/4T5R2Um.png" alt="monster0"></img>
                <img style={{ margin: "0 0" }} src="https://i.imgur.com/RZUS6Ge.png" alt="monster1"></img>
            </div>
            <div>Created By</div>
            <div>Saharat Nasahachart</div>
        </>
    )
}

export default Home
