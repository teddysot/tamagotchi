import React, { useState } from 'react'

const ShopItem = (props) => {

    const { monsterIdx, imgSrc, price, setShowModal, setSelectedItem } = props

    const [hovered, setHovered] = useState(false)

    const cardStyle = {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '15.0rem',
        height: '20.0rem',
        marginTop: '2.0rem',
        marginRight: '2.0rem',
        transform: hovered ? 'scale(1.1,1.1)' : null,
        cursor: hovered ? 'pointer' : null,
        zIndex: hovered ? '1' : '0',
        boxShadow: "rgba(0, 0, 0, 0.16) 0px 1px 4px"
    }
    return (
        <div
            style={cardStyle}
            onMouseOut={() => setHovered(false)}
            onMouseOver={() => setHovered(true)}
            onClick={() => {
                setShowModal(true)
                setSelectedItem({ imgSrc, price, monsterIdx })
            }}>
            <img style={{ width: '100%', height: '15.0rem', objectFit: 'cover' }} src={imgSrc} alt="monster0"></img>
            <div style={{ fontWeight: '600', fontSize: '1.5rem' }}>฿{price}-</div>
        </div >
    )
}

export default ShopItem
