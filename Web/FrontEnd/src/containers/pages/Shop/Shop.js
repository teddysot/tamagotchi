import React, { useState, useEffect } from 'react'
import ShopItem from '../ShopItem/ShopItem'
import { Modal, Button } from 'antd'
import Checkout from '../Checkout/Checkout'
import axios from 'axios'
import { BASE_BACKEND_URL } from '../../../config/constants';

const Shop = () => {

    const [balance, setBalance] = useState(0)

    const [showModal, setShowModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)

    const handleSuccess = () => {
        setShowModal(false)
        setBalance((balance) => balance - selectedItem.price)
    }

    const handleCancel = () => {
        setShowModal(false)
    }

    const cardStyle = {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '15.0rem',
        height: '20.0rem',
        marginTop: '2.0rem',
        marginRight: '2.0rem',
        boxShadow: "rgba(0, 0, 0, 0.16) 0px 1px 4px"
    }

    const getBalance = () => {
        axios.get(`${BASE_BACKEND_URL}:5000/tamagotchi-codecamp/us-central1/api/balance?username=teddysot`)
            .then((res) => {
                setBalance(res.data.balance)
            })
            .catch((err) => {
                console.log(err);
            })
    }

    useEffect(() => {
        getBalance()
        return () => {
            
        }
    }, [balance])

    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            {selectedItem ?
                <Modal
                    title="Do you want to purchase this monster?"
                    visible={showModal}
                    onCancel={() => { setShowModal(false) }}
                    footer={[
                        <Checkout key={0} item={selectedItem} handleSuccess={handleSuccess} />,
                        <Button key="cancel" onClick={handleCancel} >
                            Cancel
                </Button>,
                    ]}
                    bodyStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                >
                    <div
                        style={cardStyle}>
                        <img style={{ width: '100%', height: '15.0rem', objectFit: 'cover' }} src={selectedItem.imgSrc} alt="monster0"></img>
                        <div style={{ fontWeight: '600', fontSize: '1.5rem' }}>฿{selectedItem.price}-</div>
                    </div >
                </Modal> : null}
            <div style={{ marginTop: '1.0rem', padding: '2.0rem', boxShadow: "rgba(0, 0, 0, 0.16) 0px 1px 4px", fontSize: '1.1rem' }}>
                <div>Username: teddysot</div>
                <div>Balance: ฿{balance}</div>
            </div>
            <div style={{ display: 'flex', justifyContent: 'center', width: '80vw', height: '70vh', marginTop: '1.0rem' }}>
                <ShopItem imgSrc="https://i.imgur.com/4T5R2Um.png" monsterIdx={1} price={100} setShowModal={setShowModal} setSelectedItem={setSelectedItem} />
                <ShopItem imgSrc="https://i.imgur.com/RZUS6Ge.png" monsterIdx={2} price={200} setShowModal={setShowModal} setSelectedItem={setSelectedItem} />
            </div>
        </div>
    )
}

export default Shop
