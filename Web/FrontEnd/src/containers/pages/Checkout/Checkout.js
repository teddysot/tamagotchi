import React, { useState } from "react";
import { Button } from 'antd'
import './Checkout.css'
import Script from 'react-load-script'
import { publicKey } from '../../../confidentials/omise_key'
import axios from 'axios'
import { BASE_BACKEND_URL } from '../../../config/constants';



const Checkout = (props) => {

    const { item, handleSuccess } = props

    const [scriptLoaded, setScriptLoaded] = useState(false)

    let OmiseCard = null

    const handleScriptLoad = () => {
        OmiseCard = window.OmiseCard

        OmiseCard.configure({
            publicKey,
            currency: 'thb',
            frameLabel: 'Tamagotchi Shop',
            submitLabel: 'PAY NOW',
            buttonLabel: 'Pay with Omise'
        });
    }

    const creditCardConfigure = () => {
        OmiseCard.configure({
            defaultPaymentMethod: 'credit_card',
            otherPaymentMethods: []
        });


        OmiseCard.configureButton('#credit-card');

        OmiseCard.attach();
    }

    const handlePayClick = (e) => {
        e.preventDefault()
        creditCardConfigure()
        omiseCardHandler()
    }

    const omiseCardHandler = () => {
        OmiseCard.open({
            frameDescription: 'Invoice #3847',
            amount: item.price * 100,
            submitFormTarget: '#checkout-form',
            onCreateTokenSuccess: (token) => {
                axios.post(`${BASE_BACKEND_URL}:5000/tamagotchi-codecamp/us-central1/api/checkout`, { amount: item.price * 100, token, monsterIdx: item.monsterIdx })
                    .then((res) => {
                        handleSuccess()
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            },
            onFormClosed: () => {
                /* Handler on form closure. */
                OmiseCard = null
            },
        })
    }

    return (
        <div className="own-form">
            < Script
                url="https://cdn.omise.co/omise.js"
                onLoad={() => { setScriptLoaded(true) }}
            />
            <form>
                <Button id='credit-card' key="confirm" type="primary" disabled={!scriptLoaded} onClick={(e) => {
                    handleScriptLoad()
                    handlePayClick(e)
                }}>
                    Checkout
                </Button>
            </form>
        </div>
    )
}

export default Checkout
