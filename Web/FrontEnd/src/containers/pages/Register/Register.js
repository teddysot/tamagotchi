import React from 'react';
import { Row, Col, Button, Form, Input, notification, Radio } from "antd";
import { } from "@ant-design/icons";
import axios from 'axios';
import { BASE_BACKEND_URL } from '../../../config/constants';


const layout = {
    labelCol: { xs: 24, sm: 5, md: 4, lg: 5, xl: 5, xxl: 5 },
    wrapperCol: { xs: 24, sm: 19, md: 20, lg: 19, xl: 19, xxl: 19 },
};

function Register(props) {

    const onFinish = ({ username, password, character }) => {

        console.log(`${username} ${password} ${character}`);

        axios.post(`${BASE_BACKEND_URL}:5000/tamagotchi-codecamp/us-central1/api/register`, {
            username, password, character
        })
            .then(res => {
                notification.success({
                    description: "Signup successfully"
                });
            })
            .catch(err => {
                console.log(err);
                notification.error({
                    description: "Signup Failed"
                });
            });
    };

    return (
        <Row style={{ height: "100%", width: "100%", justifyContent: "center", alignItems: "center" }}>
            <Col span={16} className="Form">
                <Row style={{ height: "100%", width: "100%", justifyContent: "center", alignItems: "center", flexDirection: 'column' }}>
                    <img style={{ margin: "50px 0", objectFit: 'contain' }} src="https://static.wikia.nocookie.net/tamagotchi/images/7/74/Tamagotchi%21_logo.gif/revision/latest?cb=20110922190716" alt="tamagotchi"></img>
                    <Form
                        style={{ width: "80%", padding: "20px" }}
                        {...layout}
                        name="register"
                        onFinish={onFinish}
                        scrollToFirstError
                    >
                        <Form.Item
                            name="username"
                            label="Username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Username!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            label="Password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            name="confirm"
                            label="Confirm Password"
                            dependencies={['password']}
                            hasFeedback
                            rules={[
                                {
                                    required: true,
                                    message: 'Please confirm your password!',
                                },
                                ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                        if (!value || getFieldValue('password') === value) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject('The two passwords that you entered do not match!');
                                    },
                                }),
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item
                            name="character"
                            label="Choose Character"
                            rules={[{ required: true, message: 'Please choose a character!' }]}
                        >
                            <Radio.Group>
                                <Radio.Button value={1} style={{ height: "100px" }}>
                                    <img style={{ width: '100px', height: '100px' }} src="https://i.imgur.com/4T5R2Um.png" alt="monster0"></img>
                                </Radio.Button>
                                <Radio.Button value={2} style={{ height: "100px" }}>
                                    <img style={{ width: '100px', height: '100px' }} src="https://i.imgur.com/RZUS6Ge.png" alt="monster1"></img>
                                </Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Row justify="center" style={{ marginTop: "10px" }}>
                            <Button type="primary" htmlType="submit">
                                Register
                            </Button>
                        </Row>
                    </Form>
                </Row>
            </Col>
        </Row>
    );
}

export default Register;